# EthMotionPictures

Video streaming through **Ethereum** smart contracts
This is **ETHBerlinZwei** work submission. We did video streaming through smart contract running on top of **SKALE** blockchain.

![EthMotionPictures](how-it-looks-like.png)

NOTE: The **EthMotionPictures** project is experimental and created for **ETHBerlinZwei** only.

DEVPOST: https://devpost.com/software/ethmotionpictures

YOUTUBE: https://www.youtube.com/watch?v=Zgf13lSnMFA&feature=youtu.be

GITLAB: git clone https://gitlab.com/ls13fy/ethmotionpictures

SMART CONTRACT SOURCE CODE: https://gitlab.com/ls13fy/ethmotionpictures/blob/master/contracts/contract.sol

DEPLOYED SMART CONTRACT ADDRESSES: **0x5a2ead0bbdd9f2be43793febfdd5476357d91df2** and **0x3feb35fdfeab024e13e93b7115c3c2e4fe741146**

SKALE TEST CHAIN: http://165.22.164.226:10013

## Problem

Smart contracts are limited to the types and bandwidth of data they can process. We wanted to explore whether we could pass higher bandwidth and near real time data between two smart contracts using **SKALE Layer 2 technology** so dApp developers can use more types of data feeds in a decentralized way.

## Inspiration

We are trying to do what is, probably, never done before.

## What it does

Streams videos between two points through **Ethereum** smart contracts.

## How we built it

We captured video via client side **WebRTC** components of modern web browsers.
Then we streamed video frame portions to published smart contracts.
Then we broadcasted video streams to all the connected endpoints.

## Challenges we ran into

WiFi bandwidth was a key limitation to development and testing. More importantly, speed limitation of **Mainnet**, **Rinkeby**, **Ropsten**, **Kovan**... It doesn't allow to build our software. These networks use block timestamps based on 1 second time resolution. So, blocks cannot appear more often than 1 second there. This is not appropriate for video streaming, Thanks to **SKALE**, their **Ethereum** implementation using millisecond time resolution for mined blocks what allows 1000 video frames to be published during 1 second of time flow. So we run on top of **SKALE** implementation of Ethereum blockchain.

## Accomplishments that we're proud of

We tried to be brave enough to make video streaming attempt through blockchain.

## What we learned

We discovered video streaming through blockchain is possible.

## What's next for EthMotionPictures

Add sound support, stream higher resolution video and better time synchronization.
