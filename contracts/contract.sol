pragma solidity ^0.5.1;
//pragma experimental ABIEncoderV2;

contract VideoServer {

    struct Entry {
        string name;
        uint64 nce;
        uint64 ts;
        string frame;
    }

    mapping( bytes32 => Entry ) public entries;

    bytes32[] nameHashes;

    // event onFrame (
    //     string name,
    //     uint64 nce
    //     uint64 ts,
    //     );

    //constructor() public {
    //}

    function leave( string memory a_name ) public {
        bytes32 k = keccak256( abi.encodePacked( a_name ) );
        if( keccak256( abi.encodePacked( entries[k].name ) ) != keccak256("") ) {
            delete entries[ k ];
            uint i;
            uint cnt;
            cnt = nameHashes.length;
            for( i = 0; i < cnt; ++ i ) {
                if( nameHashes[i] == k ) {
                    if( i < (cnt-1) )
                        nameHashes[i] = nameHashes[cnt-1];
                    -- nameHashes.length;
                }
            }
        }
    }

    function push( string memory a_name, uint64 a_ts, string memory a_frame ) public {
        bytes32 k = keccak256( abi.encodePacked( a_name ) );
        if( keccak256( abi.encodePacked( entries[k].name ) ) == keccak256("") ) {
            entries[ k ] = Entry( { name: a_name, nce: 0, ts: a_ts, frame: a_frame } );
            nameHashes.push( k );
        } else {
            entries[ k ].ts = a_ts;
            ++ entries[ k ].nce;
            entries[ k ].frame = a_frame;
        }
        //emit onFrame( a_name, a_nce, a_ts );
    }

    function entriesCount() public view returns (uint cnt) {
        cnt = nameHashes.length;
    }

    function entryAt( uint i ) public view returns( string memory name, uint64 nce, uint64 ts, string memory frame ) {
        name = "";
        nce = 0;
        uint cnt;
        cnt = nameHashes.length;
        if( i < cnt ) {
            bytes32 k = nameHashes[ i ];
            name = entries[k].name;
            nce = entries[k].nce;
            ts = entries[k].ts;
            frame = entries[k].frame;
        }
    }

}
