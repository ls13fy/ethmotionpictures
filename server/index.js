let fs = require( "fs" );
let express = require( "express" );
let app = express();
let session = require( "express-session" );
const https = require( "https" );
let glob = require( "glob" );

const cc = require( "./cc.js" );
const log = require( "./log.js" );
cc.enable( true );
log.addStdout();
//log.add( strFilePath, nMaxSizeBeforeRotation, nMaxFilesCount );

log.write( cc.normal( "Starting up..." ) + "\n" );

log.write( cc.normal( "Creating web site..." ) + "\n" );
let g_nListenPort = 3001;

app.use( session( {
    "secret": "2C44-4D44-WppQ38S",
    "resave": true,
    "saveUninitialized": true
} ) );

app.use( "/app", express.static( "../client" ) );

g_bIsSSL = false;
app.listen( g_nListenPort );

log.write( cc.success( "Server is listening at " ) + cc.u( "http://localhost:" + g_nListenPort ) + "\n" );
