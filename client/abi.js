let g_joABI =



[
	{
		"constant": false,
		"inputs": [
			{
				"name": "a_name",
				"type": "string"
			}
		],
		"name": "leave",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": false,
		"inputs": [
			{
				"name": "a_name",
				"type": "string"
			},
			{
				"name": "a_ts",
				"type": "uint64"
			},
			{
				"name": "a_frame",
				"type": "string"
			}
		],
		"name": "push",
		"outputs": [],
		"payable": false,
		"stateMutability": "nonpayable",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "",
				"type": "bytes32"
			}
		],
		"name": "entries",
		"outputs": [
			{
				"name": "name",
				"type": "string"
			},
			{
				"name": "nce",
				"type": "uint64"
			},
			{
				"name": "ts",
				"type": "uint64"
			},
			{
				"name": "frame",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [],
		"name": "entriesCount",
		"outputs": [
			{
				"name": "cnt",
				"type": "uint256"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	},
	{
		"constant": true,
		"inputs": [
			{
				"name": "i",
				"type": "uint256"
			}
		],
		"name": "entryAt",
		"outputs": [
			{
				"name": "name",
				"type": "string"
			},
			{
				"name": "nce",
				"type": "uint64"
			},
			{
				"name": "ts",
				"type": "uint64"
			},
			{
				"name": "frame",
				"type": "string"
			}
		],
		"payable": false,
		"stateMutability": "view",
		"type": "function"
	}
]



;
