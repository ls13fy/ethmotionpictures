
let g_idxActiveURL = -1;
let g_arrURLs = [
    //
    // localhost/debug version
    //
    //"http://127.0.0.1:7000",
    //"https://127.0.0.1:7010"
    //
    // internet version
    //
    "http://206.189.49.21:7000",
    "https://206.189.49.21:7010"
];

let g_w3 = null;

function is_ws_url( strWeb3URL ) {
    try {
        var u = new URL( strWeb3URL );
        if ( u.protocol == "ws:" || u.protocol == "wss:" )
            return true;
    } catch ( e ) {}
    return false;
}

function do_connect( strWeb3URL, fn ) {
    fn = fn || function() {}
    g_w3 = null;
    if ( strWeb3URL == undefined || strWeb3URL == null || strWeb3URL == "" )
        return;
   console.log( "Will connect to " + strWeb3URL );
    if ( is_ws_url( strWeb3URL ) )
        g_w3 = new Web3( new Web3.providers.WebsocketProvider( strWeb3URL ) );
    else
        g_w3 = new Web3( new Web3.providers.HttpProvider( strWeb3URL ) );
    fn();
}

function get_utc_ts() {
    var ts = Math.floor( ( new Date() ).getTime() / 1000 );
    return ts;
}

function new_video_peer( strCanvasSelector, strContractAddress, strNickName, address, private_key ) {
    var joPeer = {
        "mode": "stopped",
        "periodOfTimeMilliseconds": 400,
        "strCanvasSelector": "" + strCanvasSelector,
        "elCanvas": document.querySelector( strCanvasSelector ),
        "strContractAddress": "" + strContractAddress,
        "joContract": new g_w3.eth.Contract( g_joABI, strContractAddress ),
        "strNickName": "" + strNickName,
        "address": "" + address,
        "private_key": "" + private_key,
        "do_send_frame": function( frame, fn ) {
            if( g_w3 == null )
                return;
            var self = this;
            fn = fn || function() {}
//            console.log("Will call getTransactionCount(\""+self.address+"\")...");
            g_w3.eth.getTransactionCount( self.address, function( err, nonce ) {
                if( err ) {
                    console.log("Got error from getTransactionCount(\""+self.address+"\"):", err);
                    return;
                }
               console.log("Got nonce from getTransactionCount(\""+self.address+"\"):", nonce);
                if( g_w3 == null )
                    return;
                var ts = get_utc_ts();
                let data = self.joContract.methods.push(
                    self.strNickName, ts, frame // call params
                ).encodeABI(); // the encoded ABI of the method
                var tx = new ethereumjs.Tx( {
                    "nonce": nonce,
                    "gasPrice": g_w3.utils.toHex(g_w3.utils.toWei("1", "gwei")), // 10000000000, /// g_w3.utils.toHex(g_w3.utils.toWei("20", "gwei")),
                    "gasLimit": 6000000, // 100000, ///// 282404
                    "to": self.strContractAddress,
                    "value": 0,
                    "data": data,
                } );
                tx.sign( ethereumjs.Buffer.Buffer.from( self.private_key, "hex" ) );
                var raw = "0x" + tx.serialize().toString( "hex" );
                g_w3.eth.sendSignedTransaction( raw, function( err, transactionHash ) {
                    if ( err ) {
                        console.log( "sendSignedTransaction error:", err );
                        return;
                    }
//                    console.log( transactionHash );
                    if( g_w3 == null )
                        return;
                    fn();
                } );
            } );
        },
        "do_check_nce": function( idxEntry, fn ) {
            if( g_w3 == null )
                return;
            var self = this;
            fn = fn || function() {}
            let data = self.joContract.methods.entryAt(
                idxEntry // call params
            ).call( {
                from: self.address
            }, function( err, data ) {
                if ( err )
                    return;
///                console.log( "Got frame data:", data );
                fn( data );
            } );
        },
        "do_start_streaming": function() {
            if( g_w3 == null )
                return;
            var self = this;
            self.mode = "streaming";
            self.do_streaming_frame();
        },
        "do_streaming_frame": function() {
            if( g_w3 == null )
                return;
            var self = this;
            if( self.mode != "streaming" )
                return;
            var du = extract_image_data_url();
            if ( du ) {
                // var p = document.getElementById("id_data");
                // p.innerHTML = du;

//                console.log( "Will send frame data:", du );
                self.do_send_frame( du );
                self.do_check_nce( 0, function( data ) {
                    if( g_w3 == null )
                        return;
                    self.do_process_entry( data )
                } );
            }
            setTimeout( function() {
                self.do_streaming_frame();
            }, self.periodOfTimeMilliseconds );
        },
        "do_process_entry": function( data ) {
            if( g_w3 == null )
                return;
            var self = this;
            var img = new Image();
            img.src = data.frame;
            img.onload = function() {
                var sizeImage = {
                    "x": img.width,
                    "y": img.height
                };
                var sizeReult = sizeImage;
                sizeReult.x *= 2;
                sizeReult.y *= 2;
                self.elCanvas.width = sizeReult.x;
                self.elCanvas.height = sizeReult.y;
                var ctx = self.elCanvas.getContext("2d");
                ctx.width = sizeReult.x;
                ctx.height = sizeReult.y;
                ctx.drawImage(img, 0, 0, sizeImage.x, sizeImage.y);
            };
        },
        "do_start_receiving": function() {
            if( g_w3 == null )
                return;
            var self = this;
            self.mode = "receiving";
            self.do_receiving_frame();
        },
        "do_receiving_frame": function() {
            if( g_w3 == null )
                return;
            var self = this;
            if( self.mode != "receiving" )
                return;
            self.do_check_nce( 0, function( data ) {
                self.do_process_entry( data )
            } );
            setTimeout( function() {
                self.do_receiving_frame();
            }, self.periodOfTimeMilliseconds );
        }
    };
    return joPeer;
}

let g_idxPeerMe = -1;
let g_arrVideoPeers = [];
