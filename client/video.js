let g_bEnableAudio = false;
let g_elVideoExtractor = null;
let g_elCanvasExtractor = null;
let g_elSelectAudioIn = null;
let g_elSelectAudioOut = null;
let g_elSelectVideoIn = null;
let g_arrSelectorElements = [];

function scaleTo(joSize, joMax) {
    var joResult = {
        "x": 0 + joSize.x,
        "y": 0 + joSize.y
    };
    if (joResult.x > joMax.x) {
        var k = joMax.x / joResult.x;
        joResult.x *= k;
        joResult.y *= k;
    }
    if (joResult.y > joMax.y) {
        var k = joMax.y / joResult.y;
        joResult.x *= k;
        joResult.y *= k;
    }
    return joResult;
}

let g_sizeCanvasMax = {
    "x": 160,
    "y": 100
};

function extract_image_data_url() {
    var du = null;
    if ( g_elVideoExtractor && g_elCanvasExtractor && g_elVideoExtractor.videoWidth && g_elVideoExtractor.videoHeight ) {
        var sizeVideo = {
            "x": g_elVideoExtractor.videoWidth,
            "y": g_elVideoExtractor.videoHeight
        };
        var sizeResult = scaleTo( sizeVideo, g_sizeCanvasMax );
        g_elCanvasExtractor.width = sizeResult.x;
        g_elCanvasExtractor.height = sizeResult.y;
        var ctx = g_elCanvasExtractor.getContext( "2d" );
        ctx.width = sizeResult.x;
        ctx.height = sizeResult.y;
        ctx.drawImage( g_elVideoExtractor, 0, 0, sizeResult.x, sizeResult.y );
        //
        du = g_elCanvasExtractor.toDataURL( "image/jpeg" ); // image/png"
    }
    return du;
}

function onGotAvailableDevices( deviceInfos ) {
    const values = g_arrSelectorElements.map( select => select.value );
    g_arrSelectorElements.forEach( select => {
        while ( select.firstChild ) {
            select.removeChild( select.firstChild );
        }
    } );
    for ( let i = 0; i !== deviceInfos.length; ++i ) {
        const deviceInfo = deviceInfos[ i ];
        const option = document.createElement( "option" );
        option.value = deviceInfo.deviceId;
        if ( deviceInfo.kind === "audioinput" ) {
            if ( g_bEnableAudio ) {
                option.text = deviceInfo.label || `microphone ${g_elSelectAudioIn.length + 1}`;
                g_elSelectAudioIn.appendChild( option );
            }
        } else if ( deviceInfo.kind === "audiooutput" ) {
            if ( g_bEnableAudio ) {
                option.text = deviceInfo.label || `speaker ${g_elSelectAudioOut.length + 1}`;
                g_elSelectAudioOut.appendChild( option );
            }
        } else if ( deviceInfo.kind === "videoinput" ) {
            option.text = deviceInfo.label || `camera ${g_elSelectVideoIn.length + 1}`;
            g_elSelectVideoIn.appendChild( option );
        } else {
            console.log( "Some other kind of source/device: ", deviceInfo );
        }
    }
    g_arrSelectorElements.forEach( ( select, selectorIndex ) => {
        if ( Array.prototype.slice.call( select.childNodes ).some( n => n.value === values[ selectorIndex ] ) ) {
            select.value = values[ selectorIndex ];
        }
    } );
}

function attachSinkId( element, sinkId ) {
    // attach audio output device to video element using device/sink ID.
    if ( typeof element.sinkId !== "undefined" ) {
        element.setSinkId( sinkId )
            .then( () => {
                console.log( `Success, audio output device attached: ${sinkId}` );
            } )
            .catch( error => {
                let errorMessage = error;
                if ( error.name === "SecurityError" ) {
                    errorMessage = `You need to use HTTPS for selecting audio output device: ${error}`;
                }
                console.error( errorMessage );
                if ( g_bEnableAudio )
                    g_elSelectAudioOut.selectedIndex = 0; // jump back to first output device in the list as it's the default.
            } );
    } else {
        console.warn( "Browser does not support output device selection." );
    }
}

function onChangeAudioDestination() {
    if ( !g_bEnableAudio )
        return;
    const audioDestination = g_elSelectAudioOut.value;
    attachSinkId( g_elVideoExtractor, audioDestination );
}

function onGotAvailableStream( stream ) {
    window.stream = stream; // make stream available to console
    g_elVideoExtractor.srcObject = stream;
    // Refresh button list in case labels have become available
    return navigator.mediaDevices.enumerateDevices();
}

function onHandleMultimediaError( error ) {
    console.log( "navigator.MediaDevices.getUserMedia error: ", error.message, error.name );
}

function onStartMultimedia() {
    if ( window.stream ) {
        window.stream.getTracks().forEach( track => {
            track.stop();
        } );
    }
    const audioSource = g_bEnableAudio ? g_elSelectAudioIn.value : null;
    const videoSource = g_elSelectVideoIn.value;
    let constraints = {
        audio: {
            deviceId: audioSource ? {
                exact: audioSource
            } : undefined
        },
        video: {
            deviceId: videoSource ? {
                exact: videoSource
            } : undefined
        }
    };
    if ( !g_bEnableAudio )
        delete constraints.audio;
    navigator.mediaDevices.getUserMedia( constraints ).then( onGotAvailableStream ).then( onGotAvailableDevices ).catch( onHandleMultimediaError );
}
